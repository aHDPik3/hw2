package mephi;

/**
 * Class that represents match of hour and log priority
 */
public class HourPriorityPair {
    /**
     * Log creation hour
     */
    private int hour;
    /**
     * Log priority
     */
    private int priority;

    /**
     * Constructor
     * @param hour Log creation hour
     * @param priority Log priority
     */
    public HourPriorityPair(int hour, int priority){
        this.hour=hour;
        this.priority = priority;
    }

    /**
     * Get log creation hour
     * @return Log creation hour
     */
    public int getHour() {
        return hour;
    }

    /**
     * Get log priority
     * @return Log priority
     */
    public int getPriority() {
        return priority;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof HourPriorityPair)
        {
            HourPriorityPair hppObj = (HourPriorityPair)obj;
            return hour == hppObj.getHour() && priority == hppObj.getPriority();
        }

        else
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return priority*100+hour;
    }
}
