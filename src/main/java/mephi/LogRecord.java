package mephi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 *
 * class for storing data about single log record
 */
public class LogRecord {
    /**
     * Collection of matches for log levels and their respective names
     */
    private static HashMap<String, Integer> logLevels;

    /**
     * Method to initialize match collection
     */
    public static void initLevels(){
        logLevels = new HashMap<>();
        logLevels.put("debug",7);
        logLevels.put("info",6);
        logLevels.put("notice",5);
        logLevels.put("warning",4);
        logLevels.put("warn",4);
        logLevels.put("error",3);
        logLevels.put("err",3);
        logLevels.put("crit",2);
        logLevels.put("alert",1);
        logLevels.put("emerg",0);
        logLevels.put("panic",0);
    }

    /**
     * log record priority
     */
    private int priorityId;
    /**
     * log message content (not implemented)
     */
    private String message;
    /**
     * Log raw content
     */
    private String fullLog;
    /**
     * Log record creation date
     */
    private Date date;

    /**
     * Constructor
     * @param fullLog Log raw content
     */
    public LogRecord(String fullLog) {
        this.fullLog = fullLog;
        int start=0;
        int pos =fullLog.indexOf(" ");
        pos = fullLog.indexOf(" ", pos+1);
        pos = fullLog.indexOf(" ", pos+1);
        String dateString = fullLog.substring(start,pos);
        start = fullLog.indexOf("<");
        pos = fullLog.indexOf(">",start);
        String logLevel = fullLog.substring(start+1,pos);
        if(logLevels==null)
            initLevels();
        priorityId = logLevels.get(logLevel);
        DateFormat format = new SimpleDateFormat("MMM dd HH:mm:ss", Locale.ENGLISH);
        try {
            date = format.parse(dateString);
        }
        catch(Exception ex) {}
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        cal.setTime(date); // your date (java.util.Date)
        cal.set(Calendar.YEAR, year);
        date = cal.getTime();
    }

    /**
     * Get message content (not implemented)
     * @return Message content
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get message creation date
     * @return Message creation date
     */
    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return fullLog;
    }

    /**
     * Get log record priority
     * @return Log record priority
     */
    public int getPriorityId() {
        return priorityId;
    }

    /**
     * Convert LogRecord To HourPriorityPair object
     * @return Corresponding HourPriorityPair object
     */
    public HourPriorityPair toHourPriorityPair(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        return new HourPriorityPair(hour,priorityId);
    }
}


