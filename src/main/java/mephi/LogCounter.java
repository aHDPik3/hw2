package mephi;

import org.apache.ignite.*;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.compute.ComputeJob;
import org.apache.ignite.compute.ComputeJobAdapter;
import org.apache.ignite.compute.ComputeJobResult;
import org.apache.ignite.compute.ComputeTaskSplitAdapter;
import org.apache.ignite.configuration.CacheConfiguration;
import org.jetbrains.annotations.Nullable;

import javax.cache.Cache;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;

/**
 * Main class
 */
public class LogCounter {

    /**
     * Main function
     * @param args List of arguments
     * @throws IgniteException
     * @throws IOException
     * @throws ParseException
     */
    public static void main(String[] args) throws IgniteException, IOException, ParseException {
        if (args.length > 0 && ("store".equals(args[0]) || "compute".equals(args[0]))) {
            Ignition.setClientMode(true);
            LogRecord.initLevels();
            try (Ignite ignite = Ignition.start(args[1])) {
                ignite.active(true);
                CacheConfiguration<Integer, String> cacheCfg = new CacheConfiguration<>("myCache");
                cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
                cacheCfg.setBackups(1);
                cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
                cacheCfg.setIndexedTypes(Integer.class, String.class);
                // Put values in cache.
                IgniteCache<Integer, String> cache = ignite.getOrCreateCache(cacheCfg);
                if(args[0].equals("store")) {
                    System.out.println("Stroring logs from "+args[2]);
                    Path filePath = Paths.get(args[2]);
                    try (Scanner scaner = new Scanner(filePath);
                         IgniteDataStreamer<Integer, String> streamer = ignite.dataStreamer("myCache")) {
                        int i = cache.size()+1;
                        while (scaner.hasNextLine()) {
                            streamer.addData(i, scaner.nextLine());
                            i++;
                        }
                    }
                    System.out.println("Store completed");
                }
                else {
                    System.out.println("Computing log count");
                    if(cache.size()==0)
                        System.out.println("No logs found");
                    else {
                        IgniteCompute compute = ignite.compute();
                        Map<HourPriorityPair, Integer> res = compute.execute(LogCountTask.class, cache);
                        for (Map.Entry<HourPriorityPair, Integer> val : res.entrySet()) {
                            System.out.println("priority id - " + val.getKey().getPriority() + " hour - " + val.getKey().getHour() + " count - " + val.getValue());
                        }
                    }
                    System.out.println("Computing finished");
                }
            }

        } else {
            System.out.println("incorrect parameters");
        }
    }

    /**
     * Class for ignite compute task execution
     */
    static class LogCountTask extends ComputeTaskSplitAdapter<IgniteCache<Integer, String>, Map<HourPriorityPair, Integer>> {


        @Override
        protected Collection<? extends ComputeJob> split(int i, IgniteCache<Integer, String> entries) throws IgniteException {
            List<ComputeJob> jobs = new ArrayList<>(entries.size());
            for (final Cache.Entry<Integer, String> logRecord : entries) {
                jobs.add(new ComputeJobAdapter() {
                    @Override
                    public Object execute() throws IgniteException {
                        LogRecord logRec = new LogRecord(logRecord.getValue());
                        return logRec.toHourPriorityPair();
                    }
                });
            }
            return jobs;
        }

        @Nullable
        @Override
        public Map<HourPriorityPair, Integer> reduce(List<ComputeJobResult> list) throws IgniteException {
            Map<HourPriorityPair, Integer> logStats = new HashMap<>();
            for (ComputeJobResult res : list) {
                HourPriorityPair hpp = res.<HourPriorityPair>getData();
                Integer count = logStats.get(hpp);
                if (count == null)
                    count = 0;
                logStats.put(hpp, count + 1);

            }
            return logStats;
        }
    }
}
