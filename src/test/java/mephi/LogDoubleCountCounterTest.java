package mephi;

import org.apache.ignite.*;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Test class for checking repeatable entries
 */
public class LogDoubleCountCounterTest {
    /**
     * Ignite connection
     */
    private Ignite ignite = null;
    /**
     * Ingine compute object
     */
    private IgniteCompute compute = null;
    /**
     * Ignite storage
     */
    private IgniteCache<Integer, String> cache = null;

    /**
     * Test initialization
     */
    @Before
    public void initialize() {
        try {
            Ignition.setClientMode(true);
            LogRecord.initLevels();
            ignite = Ignition.start("config.xml");
            ignite.active(true);
            compute = ignite.compute();
            CacheConfiguration<Integer, String> cacheCfg = new CacheConfiguration<>("testCache");
            cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
            cacheCfg.setBackups(1);
            cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
            cacheCfg.setIndexedTypes(Integer.class, String.class);
            cache = ignite.getOrCreateCache(cacheCfg);
            cache.clear();
        } catch (Exception e) {

            System.out.println(e);
        }
    }

    private void generateDoubleDifferentCache() {
        try(IgniteDataStreamer<Integer,String> streamer = ignite.dataStreamer("testCache")) {
            streamer.addData(1, "Dec 02 01:21:39 Andrew Hadoop: <panic> etc folder not found shutting down");
            streamer.addData(2, "Dec 02 01:23:42 Andrew Firefox: <emerg> application started");
        }
    }

    @Test
    public void testDoubleDifferentLog() {
        generateDoubleDifferentCache();

        // Execute task on the cluster and wait for its completion.
        // List of Surveys are passed to CalculateSurveyTask
        Map<HourPriorityPair, Integer> res = compute.execute(LogCounter.LogCountTask.class, cache);
        assert res.containsKey(new HourPriorityPair(1, 0));
        assert res.get(new HourPriorityPair(1, 0)) == 2;
    }
    /**
     * Clean up resources
     */
    @After
    public void tearDown() {
        ignite.close();
    }
}
